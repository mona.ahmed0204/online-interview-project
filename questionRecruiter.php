<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Questions</title>
    <link rel="stylesheet" href="styles/bootstrap4/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="styles/dashboard_styles.css">
    <link rel="stylesheet" type="text/css" href="styles/responsive.css">
    <link rel="stylesheet" href="styles/style.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css" rel="stylesheet"> 
    <script src="js/jquery-3.4.1.min.js"></script>
    <link rel="stylesheet" href="styles/bootstrap4/popper.js">
       
     <!-- video and audio -->
    <link href="styles/video-js.min.css" rel="stylesheet">
    <link href="styles/videojs.wavesurfer.min.css" rel="stylesheet">
    <link href="styles/videojs.record.min.css" rel="stylesheet">
    <link href="styles/examples.css" rel="stylesheet">
    <script src="js/video.min.js"></script>
    <script src="js/RecordRTC.js"></script>
    <script src="js/adapter.js"></script>
    <script src="js/wavesurfer.min.js"></script>
    <script src="js/wavesurfer.microphone.min.js"></script>
    <script src="js/videojs.wavesurfer.min.js"></script>
    <script src="js/videojs.record.min.js"></script>
    <script src="js/browser-workarounds.js"></script>

    <!-- main -->
    <script src="styles/bootstrap4/bootstrap.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <style>
        body{
            background-color: white;
            
        }
         /* change player background color */
         #myVideo {
            background-color: #9ab87a;
            display: none;
            margin: auto;
        }
        #myAudio {
            background-color: #9FD6BA;
            display: block;
            margin: auto;
        }
        </style>

</head>
<body>
<div class="super_container">
<?php include_once("includes/header.php"); ?>
<br><br>
<br><br>
    <div class="container">
        <div class="row">
            <div class="mt-4 align-r col-lg-12">
                <img id="addbutton" src="images/add.png">
            </div>
        </div>
    </div>
   
   
    <div class="container" id="boxes">
        <div class="row mt-5 q-sec" >
            <div class="col-lg-12" id="quest1">
                <h4 class="mt-4">Question1</h4>
                <form class="">
                    <div class="form-group p-5">
                      <textarea class="form-control" id="exampleFormControlTextarea1" row="6" placeholder="Type your question here......."></textarea>
                    </div>
                  </form>
                  <!-- Audio -->
                  <audio id="myAudio" class="video-js vjs-default-skin"></audio>
                   <!-- video -->
                    <video id="myVideo" playsinline class="video-js vjs-default-skin" ></video>
                  <div id="timeline-wrap">
                    <div id="timeline"></div>
                    
                    <!-- This is the individual marker-->
                    <div class="marker mfirst timeline-icon one">
                        <i class="fas fa-microphone" onclick="audioRecord();"></i>
                    </div>
                    <!-- / marker -->
                  
                    <!-- This is the individual marker-->
                    <div class="marker m2 timeline-icon two">
                        <i class="fas fa-video" onclick="videoRecord()"></i>
                    </div>
                    <!-- / marker -->
                    <div class="marker  timeline-icon delete">
                        <i class="fas fa-trash-alt"></i>
                    </div>
                  </div>
            </div>
        </div>
    </div>
    <div class="row text-center mb-3 prev-mt align-flex mt-4">
        <button class="btn preview"> <i class="far fa-eye mr-2"></i> Preview</button>
    </div>
    <div class="row text-center align-flex mb-4">
        <button class="btn done">Done</button>
    </div>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="styles/bootstrap4/popper.js"></script>
    <script src="styles/bootstrap4/bootstrap.min.js"></script>
    <script src="plugins/greensock/TweenMax.min.js"></script>
    <script src="plugins/greensock/TimelineMax.min.js"></script>
    <script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
    <script src="plugins/greensock/animation.gsap.min.js"></script>
    <script src="plugins/greensock/ScrollToPlugin.min.js"></script>
    <script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
    <script src="plugins/easing/easing.js"></script>
    <script src="plugins/parallax-js-master/parallax.min.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/video-interview.js"></script>
    <script src="js/audio-interview.js"></script>
    <script src="js/script.js"></script>
    

    </div>
</body>
</html>