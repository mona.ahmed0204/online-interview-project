   <!-- Header -->
    
   <header class="header">
            <!-- Header Content -->
            <div class="header_container">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="header_content d-flex flex-row align-items-center justify-content-start">
                                <div class="logo_container">
                                    <a href="#">
                                        <div class="logo_text"><image src="images/logo.png"></image></div>
                                    </a>
                                </div>
                                <div class="col-lg-6 hero">
                                     <div class="input-group">
                                    <input type="text" class="form-control" placeholder="search">
                                    <div class="input-group-append">
                                      <button class="btn btn-secondary" type="button">
                                        <i class="fa fa-search"></i>
                                      </button>
                                    </div>
                                  </div>
                                </div>
                                <nav class="main_nav_contaner ml-auto">
                                    <!-- search -->
                                    <div class="row">
                                        <ul class="main_nav">
                
                                        <div class="col"><div class="panel panel-default">
                                            <div class="panel-body">
                                              <!-- Single button -->
                                              <div class="btn-group pull-right top-head-dropdown">
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-bell" aria-hidden="true"></i> <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                  <li>
                                                    <a href="#" class="top-text-block">
                                                      <div class="top-text-heading">You have <b>3 new themes</b> trending</div>
                                                      <div class="top-text-light">15 minutes ago</div>
                                                    </a> 
                                                  </li>
                                                  <li>
                                                    <a href="#" class="top-text-block">
                                                      <div class="top-text-heading">You have <b>3 new themes</b> trending</div>
                                                      <div class="top-text-light">15 minutes ago</div>
                                                    </a> 
                                                  </li>

                                                  <li>
                                                    <a href="#" class="top-text-block">
                                                      <div class="top-text-heading">You have <b>3 new themes</b> trending</div>
                                                      <div class="top-text-light">15 minutes ago</div>
                                                    </a> 
                                                  </li>
                                                  <li>
                                                    <a href="#" class="top-text-block">
                                                      <div class="top-text-heading">You have <b>3 new themes</b> trending</div>
                                                      <div class="top-text-light">15 minutes ago</div>
                                                    </a> 
                                                  </li>
                                                  <li>
                                                    <a href="#" class="top-text-block">
                                                      <div class="top-text-heading">You have <b>3 new themes</b> trending</div>
                                                      <div class="top-text-light">15 minutes ago</div>
                                                    </a> 
                                                  </li>
                                                  <li>
                                                    <a href="#" class="top-text-block">
                                                      <div class="top-text-heading">You have <b>3 new themes</b> trending</div>
                                                      <div class="top-text-light">15 minutes ago</div>
                                                    </a> 
                                                  </li>
                                                  <li>
                                                    <a href="#" class="top-text-block">
                                                      <div class="top-text-heading">You have <b>3 new themes</b> trending</div>
                                                      <div class="top-text-light">15 minutes ago</div>
                                                    </a> 
                                                  </li>



                                                 <li>
                                                  <div class="loader-topbar"></div>
                                                 </li>
                                                </ul>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <!--  -->
                                         <div class="col employer">
                                           <div class="dropdown">
    <button class="btn dropdown-toggle login" type="button" data-toggle="dropdown">
    <span class="caret"></span><img class="employ" src= "images/profile_photo.png"><span>Ahmed</span></button>
    <ul class="dropdown-menu">
      <li><a href="#">logout</a></li>
    
    </ul>
  </div>

   </div>
                                       
    </div>
                                                                
      </ul>
      <!--  -->
                                
    
                                   
	<!-- Hamburger -->

							
								<div class="hamburger menu_mm">
									<i class="fa fa-bars menu_mm" aria-hidden="true"></i>
								</div>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
    
            <!-- Header Search Panel -->
            <div class="header_search_container">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="header_search_content d-flex flex-row align-items-center justify-content-end">
                                <form action="#" class="header_search_form">
                                    <input type="search" class="search_input" placeholder="Search" required="required">
                                    <button class="header_search_button d-flex flex-column align-items-center justify-content-center">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                    
                  <div class="row">
                                        <ul class="main_nav">
                
                                        <div class="col"><div class="panel panel-default">
                                            <div class="panel-body">
                                              <!-- Single button -->
                                              <div class="btn-group pull-right top-head-dropdown">
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-bell" aria-hidden="true"></i> <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                  <li>
                                                    <a href="#" class="top-text-block">
                                                      <div class="top-text-heading">You have <b>3 new themes</b> trending</div>
                                                      <div class="top-text-light">15 minutes ago</div>
                                                    </a> 
                                                  </li>
                                                  <li>
                                                    <a href="#" class="top-text-block">
                                                      <div class="top-text-heading">You have <b>3 new themes</b> trending</div>
                                                      <div class="top-text-light">15 minutes ago</div>
                                                    </a> 
                                                  </li>

                                                  <li>
                                                    <a href="#" class="top-text-block">
                                                      <div class="top-text-heading">You have <b>3 new themes</b> trending</div>
                                                      <div class="top-text-light">15 minutes ago</div>
                                                    </a> 
                                                  </li>
                                                  <li>
                                                    <a href="#" class="top-text-block">
                                                      <div class="top-text-heading">You have <b>3 new themes</b> trending</div>
                                                      <div class="top-text-light">15 minutes ago</div>
                                                    </a> 
                                                  </li>
                                                  <li>
                                                    <a href="#" class="top-text-block">
                                                      <div class="top-text-heading">You have <b>3 new themes</b> trending</div>
                                                      <div class="top-text-light">15 minutes ago</div>
                                                    </a> 
                                                  </li>
                                                  <li>
                                                    <a href="#" class="top-text-block">
                                                      <div class="top-text-heading">You have <b>3 new themes</b> trending</div>
                                                      <div class="top-text-light">15 minutes ago</div>
                                                    </a> 
                                                  </li>
                                                  <li>
                                                    <a href="#" class="top-text-block">
                                                      <div class="top-text-heading">You have <b>3 new themes</b> trending</div>
                                                      <div class="top-text-light">15 minutes ago</div>
                                                    </a> 
                                                  </li>



                                                 <li>
                                                  <div class="loader-topbar"></div>
                                                 </li>
                                                </ul>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <!--  -->
                                         <div class="col employer">
                                           <div class="dropdown">
    <button class="btn dropdown-toggle login" type="button" data-toggle="dropdown">
    <span class="caret"></span><img class="employ" src= "images/profile_photo.png"><span>Ahmed</span></button>
    <ul class="dropdown-menu">
      <li><a href="#">logout</a></li>
    
    </ul>
  </div>

   </div>
                                       
    </div>
                </div>
           		<!--  -->
            </div>			
        </header>
    
        <!-- Menu -->
    
        <div class="menu d-flex flex-column align-items-end justify-content-start text-right menu_mm trans_400">
            <div class="menu_close_container"><div class="menu_close"><div></div><div></div></div></div>
            <div class="search">
                <form action="#" class="header_search_form menu_mm">
                    <input type="search" class="search_input menu_mm" placeholder="Search" required="required">
                    <button class="header_search_button d-flex flex-column align-items-center justify-content-center menu_mm">
                        <i class="fa fa-search menu_mm" aria-hidden="true"></i>
                    </button>
                </form>
                
            </div>
           <!--  -->

           <div class="row responsive">
           

            <div class="col"><div class="panel panel-default">
                <div class="panel-body">
                  <!-- Single button -->
                  <div class="btn-group pull-right top-head-dropdown">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-bell" aria-hidden="true"></i> <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right">
                      <li>
                        <a href="#" class="top-text-block">
                          <div class="top-text-heading">You have <b>3 new themes</b> trending</div>
                          <div class="top-text-light">15 minutes ago</div>
                        </a> 
                      </li>
                      <li>
                        <a href="#" class="top-text-block">
                          <div class="top-text-heading">You have <b>3 new themes</b> trending</div>
                          <div class="top-text-light">15 minutes ago</div>
                        </a> 
                      </li>

                      <li>
                        <a href="#" class="top-text-block">
                          <div class="top-text-heading">You have <b>3 new themes</b> trending</div>
                          <div class="top-text-light">15 minutes ago</div>
                        </a> 
                      </li>
                      <li>
                        <a href="#" class="top-text-block">
                          <div class="top-text-heading">You have <b>3 new themes</b> trending</div>
                          <div class="top-text-light">15 minutes ago</div>
                        </a> 
                      </li>
                      <li>
                        <a href="#" class="top-text-block">
                          <div class="top-text-heading">You have <b>3 new themes</b> trending</div>
                          <div class="top-text-light">15 minutes ago</div>
                        </a> 
                      </li>
                      <li>
                        <a href="#" class="top-text-block">
                          <div class="top-text-heading">You have <b>3 new themes</b> trending</div>
                          <div class="top-text-light">15 minutes ago</div>
                        </a> 
                      </li>
                      <li>
                        <a href="#" class="top-text-block">
                          <div class="top-text-heading">You have <b>3 new themes</b> trending</div>
                          <div class="top-text-light">15 minutes ago</div>
                        </a> 
                      </li>



                     <li>
                      <div class="loader-topbar"></div>
                     </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <!--  -->
             <div class="col employer">
               <div class="dropdown">
<button class="btn dropdown-toggle login" type="button" data-toggle="dropdown">
<span class="caret"></span><img class="employ" src= "images/profile_photo.png"><span>Ahmed</span></button>
<ul class="dropdown-menu">
<li><a href="#">logout</a></li>


</div>

</div>
           
</div>

           
        </div>