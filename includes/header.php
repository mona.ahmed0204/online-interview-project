<nav class="navbar navbar-expand-lg navbar-light fixed-top">
        <a class="navbar-brand" href="#"><img src="images/logo.png"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <div id="search-input" class="input-group">
                <input type="text" class="form-control" placeholder="Search">
                <div class="input-group-append">
                  <button class="btn-search btn btn-secondary" type="button">
                    <i class="fa fa-search"></i>
                  </button>
                </div>
              </div>
          </ul>

          <div class="btn-group pull-right top-head-dropdown bell">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           <!-- notification -->
            <i class="fa fa-bell" aria-hidden="true"></i>
            </button>
            <ul id="list" class="dropdown-menu dropdown-menu-right">
                <li class="notification-shaded">برجاء ملاحظة خروج المهندس  Helmy mohamed خارج نطاق الـ 5 كيلو</li><li class="notification-shaded">برجاء ملاحظة خروج المهندس  Maboalhasan خارج نطاق الـ 5 كيلو</li><li class="notification-shaded">برجاء ملاحظة خروج المهندس  Yosaf Ahmad خارج نطاق الـ 5 كيلو</li>
                <li class="notification-shaded">برجاء ملاحظة خروج المهندس  abdelhadyahmed خارج نطاق الـ 5 كيلو</li>
                <li class="notification-shaded">برجاء ملاحظة خروج المهندس  afifi خارج نطاق الـ 5 كيلو</li>
                <li class="notification-shaded">برجاء ملاحظة خروج المهندس  ahmed خارج نطاق الـ 5 كيلو</li>
                <li class="notification-shaded">برجاء ملاحظة خروج المهندس  ahmed elsayed abdelhady خارج نطاق الـ 5 كيلو</li>
                <li class="notification-shaded">برجاء ملاحظة خروج المهندس  ahmed gamal خارج نطاق الـ 5 كيلو</li>
                <li class="notification-shaded">برجاء ملاحظة خروج المهندس  ahmed magdy خارج نطاق الـ 5 كيلو</li>
                <li class="notification-shaded">برجاء ملاحظة خروج المهندس  ahmed ragab خارج نطاق الـ 5 كيلو</li>
                <li class="notification-shaded">برجاء ملاحظة خروج المهندس  ahmed shaker خارج نطاق الـ 5 كيلو</li>
                <li class="notification-shaded">برجاء ملاحظة خروج المهندس  ahmed wael خارج نطاق الـ 5 كيلو</li>
                <li class="notification-shaded">برجاء ملاحظة خروج المهندس  ahmed_khaled خارج نطاق الـ 5 كيلو</li>
                <li class="notification-shaded">برجاء ملاحظة خروج المهندس  ahmeddiaaeldeen خارج نطاق الـ 5 كيلو</li>
                <li class="notification-shaded">برجاء ملاحظة خروج المهندس  ahmedel sayed خارج نطاق الـ 5 كيلو</li>
            </ul>
         </div>

         <div class="dropdown btn-drop">
            <button class="btn dropdown-toggle login" type="button" data-toggle="dropdown">
            <span class="caret"></span><img class="employ" src="images/profile_photo.png"><span>Ahmed</span></button>
            <ul class="dropdown-menu login-dm">
              <li class=" dropdown-item">logout</li>
              <li class="dropdown-item">Profile</li>

            </ul>
          </div>
        </div>
      </nav>