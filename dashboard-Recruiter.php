<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>online-interview</title>
    <link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<!-- <link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css" rel="stylesheet"> 
<link rel="stylesheet" type="text/css" href="styles/dashboard_styles.css">
<link rel="stylesheet" type="text/css" href="styles/responsive.css">
<link rel="stylesheet" type="text/css" href="styles/dashboard-re.css">

<style>
  
</style>
</head>
<body>
    <div class="super_container">
     <?php include_once("includes/header.php"); ?>
        <!-- dashboard -->
        <br><br><br><br><br><br>
        
     <div class="container">
        
     </div>
        <div class="container">
          <div class="row btn-row">
            <button class="btn two warn mr-4 mb-3">Add a job</button>
          </div>
            <div class="row">
             <div class="col-lg-4">  <div id="timeline" class="timeline-outer">
                <div class="bg-title"><h3 class="job" >Sales Executive</h3>
                    <span class="three-EGY">left 3 Days</span>
                 
                <p class="sales">22 Interviewer</p>
                </div>
                <div class="dropdown modi">
                    <button class="btn btn-v" class="btn dropdown-toggle" data-toggle="dropdown">
                      <i class="fas fa-ellipsis-v"></i>
                    </button>
                    <div class="dropdown-menu modifyied">
                      <a class="dropdown-item" href="#">Modify vacancy</a>
                      <a class="dropdown-item" href="#">Modify questions</a>
                      <a class="dropdown-item" href="#">delete</a>
                    </div>
                  </div>
                <div class="container" id="content">
                  
                  <div class="row employs ">
                    
                    <div class="col s12 m12 l12 con-h">
                     
                      <ul class="line mt-3">
                       
                       <div class="row">
                        <div class="heros">
                          <p class="p1">0</p>
                          <p class="p1">Not Watched</p>
                        </div>
                        <div class=" heros">
                          <p class="p2">3</p>
                          <p class="p2">Acceptable</p>
                        </div>

                       </div>
                     
                       <div class="row">
                        <div class="heros">
                          <p class="p3">5</p>
                          <p class="p3">shortlist</p>
                        </div>
                        <div class="heros">
                          <p class="p4">0</p>
                          <p class="p4">Ignore</p>
                        </div>

                       </div>
                       
                    </ul>
                    </div>
                  </div>
                </div>
              </div>
             
              <br/>
            </div>
        <!-- 2 -->
        <div class="col-lg-4">  <div id="timeline" class="timeline-outer">
          <div class="bg-title"><h3 class="job" >Sales Executive</h3>
              <span class="three-EGY">left 3 Days</span>
           
          <p class="sales">22 Interviewer</p>
          </div>
          <div class="dropdown  modi">
            <button class="btn btn-v" class="btn dropdown-toggle" data-toggle="dropdown">
            <i class="fas fa-ellipsis-v"></i>
          </button>
              <div class="dropdown-menu modifyied">
                <a class="dropdown-item" href="#">Modify vacancy</a>
                <a class="dropdown-item" href="#">Modify questions</a>
                <a class="dropdown-item" href="#">delete</a>
              </div>
            </div>
          <div class="container" id="content">
            
            <div class="row employs ">
              
              <div class="col s12 m12 l12 con-h">
               
                <ul class="line mt-3">
                 
                 <div class="row">
                  <div class="shadow heros">
                    <p class="p1">6</p>
                    <p class="p1">Not Watched</p>
                  </div>
                  <div class=" heros">
                    <p class="p2">3</p>
                    <p class="p2">Acceptable</p>
                  </div>

                 </div>
               
                 <div class="row">
                  <div class="heros">
                    <p class="p3">5</p>
                    <p class="p3">shortlist</p>
                  </div>
                  <div class="heros">
                    <p class="p4">0</p>
                    <p class="p4">Ignore</p>
                  </div>

                 </div>
                 
              </ul>
              </div>
            </div>
          </div>
        </div>
       
        <br/>
      </div>
        <!-- 3 -->
        <div class="col-lg-4">  <div id="timeline" class="timeline-outer">
          <div class="bg-title"><h3 class="job" >Sales Executive</h3>
              <span class="three-EGY">left 3 Days</span>
          <p class="sales">22 Interviewer</p>
          </div>
          <div class="dropdown  modi">
            <button class="btn btn-v" class="btn dropdown-toggle" data-toggle="dropdown">
              <i class="fas fa-ellipsis-v"></i>
            </button>
              <div class="dropdown-menu modifyied">
                <a class="dropdown-item" href="#">Modify vacancy</a>
                <a class="dropdown-item" href="#">Modify questions</a>
                <a class="dropdown-item" href="#">delete</a>
              </div>
            </div>
          <div class="container" id="content">
            
            <div class="row employs ">
              
              <div class="col s12 m12 l12 con-h">
               
                <ul class="line mt-3">
                 
                 <div class="row">
                  <div class="shadow heros">
                    <p class="p1">10</p>
                    <p class="p1">Not Watched</p>
                  </div>
                  <div class=" heros">
                    <p class="p2">3</p>
                    <p class="p2">Acceptable</p>
                  </div>

                 </div>
               
                 <div class="row">
                  <div class="heros">
                    <p class="p3">5</p>
                    <p class="p3">shortlist</p>
                  </div>
                  <div class="heros">
                    <p class="p4">0</p>
                    <p class="p4">Ignore</p>
                  </div>

                 </div>
                 
              </ul>
              </div>
            </div>
          </div>
        </div>
       
        <br/>
      </div>
        </div>
        <!-- 2 -->
          <!-- dashboard -->
        
          
     <!--  -->
        
            <div class="row">
              <div class="col-lg-4">  <div id="timeline" class="timeline-outer">
                <div class="bg-title"><h3 class="job" >Sales Executive</h3>
                    <span class="three-EGY">left 3 Days</span>
                 
                <p class="sales">22 Interviewer</p>
                </div>
                <div class="dropdown  modi">
                    <button class="btn btn-v" class="btn dropdown-toggle" data-toggle="dropdown">
                      <i class="fas fa-ellipsis-v"></i>
                    </button>
                    <div class="dropdown-menu modifyied">
                      <a class="dropdown-item" href="#">Modify vacancy</a>
                      <a class="dropdown-item" href="#">Modify questions</a>
                      <a class="dropdown-item" href="#">delete</a>
                    </div>
                  </div>
                <div class="container" id="content">
                  
                  <div class="row employs ">
                    
                    <div class="col s12 m12 l12 con-h">
                     
                      <ul class="line mt-3">
                       
                       <div class="row">
                        <div class=" heros">
                          <p class="p1">0</p>
                          <p class="p1">Not Watched</p>
                        </div>
                        <div class=" heros">
                          <p class="p2">3</p>
                          <p class="p2">Acceptable</p>
                        </div>

                       </div>
                     
                       <div class="row">
                        <div class="heros">
                          <p class="p3">5</p>
                          <p class="p3">shortlist</p>
                        </div>
                        <div class="heros">
                          <p class="p4">0</p>
                          <p class="p4">Ignore</p>
                        </div>

                       </div>
                       
                    </ul>
                    </div>
                  </div>
                </div>
              </div>
             
              <br/>
            </div>
        <!-- 2 -->
        <div class="col-lg-4">  <div id="timeline" class="timeline-outer">
          <div class="bg-title"><h3 class="job" >Sales Executive</h3>
              <span class="three-EGY">left 3 Days</span>
           
          <p class="sales">22 Interviewer</p>
          </div>
          <div class="dropdown  modi">
              <button class="btn btn-v" class="btn dropdown-toggle" data-toggle="dropdown">
                <i class="fas fa-ellipsis-v"></i>
              </button>
              <div class="dropdown-menu modifyied">
                <a class="dropdown-item" href="#">Modify vacancy</a>
                <a class="dropdown-item" href="#">Modify questions</a>
                <a class="dropdown-item" href="#">delete</a>
              </div>
            </div>
          <div class="container" id="content">
            
            <div class="row employs ">
              
              <div class="col s12 m12 l12 con-h">
               
                <ul class="line mt-3">
                 
                 <div class="row">
                  <div class="shadow heros">
                    <p class="p1">10</p>
                    <p class="p1">Not Watched</p>
                  </div>
                  <div class=" heros">
                    <p class="p2">3</p>
                    <p class="p2">Acceptable</p>
                  </div>

                 </div>
               
                 <div class="row">
                  <div class="heros">
                    <p class="p3">5</p>
                    <p class="p3">shortlist</p>
                  </div>
                  <div class="heros">
                    <p class="p4">0</p>
                    <p class="p4">Ignore</p>
                  </div>

                 </div>
                 
              </ul>
              </div>
            </div>
          </div>
        </div>
       
        <br/>
      </div>
        <!-- 3 -->
        <div class="col-lg-4">  <div id="timeline" class="timeline-outer">
          <div class="bg-title"><h3 class="job" >Sales Executive</h3>
              <span class="three-EGY">left 3 Days</span>
           
          <p class="sales">22 Interviewer</p>
          </div>
          <div class="dropdown  modi">
              <button class="btn btn-v" class="btn dropdown-toggle" data-toggle="dropdown">
                <i class="fas fa-ellipsis-v"></i>
              </button>
              <div class="dropdown-menu modifyied">
                <a class="dropdown-item" href="#">Modify vacancy</a>
                <a class="dropdown-item" href="#">Modify questions</a>
                <a class="dropdown-item" href="#">delete</a>
              </div>
            </div>
          <div class="container" id="content">
            
            <div class="row employs ">
              
              <div class="col s12 m12 l12 con-h">
               
                <ul class="line mt-3">
                 
                 <div class="row">
                  <div class=" shadow heros">
                    <p class="p1">10</p>
                    <p class="p1">Not Watched</p>
                  </div>
                  <div class=" heros">
                    <p class="p2">3</p>
                    <p class="p2">Acceptable</p>
                  </div>

                 </div>
               
                 <div class="row">
                  <div class="heros">
                    <p class="p3">5</p>
                    <p class="p3">shortlist</p>
                  </div>
                  <div class="heros">
                    <p class="p4">0</p>
                    <p class="p4">Ignore</p>
                  </div>

                 </div>
                 
              </ul>
              </div>
            </div>
          </div>
        </div>
       
        <br/>
      </div>
         </div>
       

    </div>
<!--  -->








          
        <!-- end container -->
        </div>
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="styles/bootstrap4/popper.js"></script>
        <script src="styles/bootstrap4/bootstrap.min.js"></script>
        <script src="plugins/greensock/TweenMax.min.js"></script>
        <script src="plugins/greensock/TimelineMax.min.js"></script>
        <script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
        <script src="plugins/greensock/animation.gsap.min.js"></script>
        <script src="plugins/greensock/ScrollToPlugin.min.js"></script>
        <script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
        <script src="plugins/easing/easing.js"></script>
        <script src="plugins/parallax-js-master/parallax.min.js"></script>
        <script src="js/custom.js"></script>

       
</body>
</html>