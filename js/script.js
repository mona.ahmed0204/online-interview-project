var options = {
    controls: true,
    width: 320,
    height: 240,
    fluid: false,
    plugins: {
        record: {
            audio: true,
            video: true,
            maxLength: 10,
            debug: true
        }
    }
};

// apply some workarounds for opera browser
applyVideoWorkaround();

var player = videojs('myVideo', options, function() {
    // print version information at startup
    var msg = 'Using video.js ' + videojs.VERSION +
        ' with videojs-record ' + videojs.getPluginVersion('record') +
        ' and recordrtc ' + RecordRTC.version;
    videojs.log(msg);
});

// error handling
player.on('deviceError', function() {
    console.log('device error:', player.deviceErrorCode);
});

player.on('error', function(element, error) {
    console.error(error);
});

// user clicked the record button and started recording
player.on('startRecord', function() {
    console.log('started recording!');
});

// user completed recording and stream is available
player.on('finishRecord', function() {
    // the blob object contains the recorded data that
    // can be downloaded by the user, stored on server etc.
    console.log('finished recording: ', player.recordedData);
});
function videoRecord(){
    document.getElementById("myVideo").style.display = "block";
    document.getElementById("myAudio").style.display = "none";

}

function audioRecord(){
    document.getElementById("myAudio").style.display = "block";
    document.getElementById("myVideo").style.display = "none";

}
// copy question element
let counter =1;
let addbutton = document.getElementById("addbutton");
addbutton.addEventListener("click", function() {
  let questCounter = counter + 1;
  let boxes = document.getElementById("boxes");
  let clone = boxes.children[0].cloneNode(true);
  boxes.appendChild(clone);
  boxes.children[counter].children[0].setAttribute("id","quest"+ questCounter);
//   alert(boxes.children[counter].children[0].getAttribute("id"));
  let newQuest =  document.getElementById("quest"+ questCounter);
  newQuest.scrollIntoView();
  newQuest.children[0].textContent = "Question" + " " + questCounter ;
  newQuest.children[1].children[0].children[0].textContent = "";
counter++
});


    // var c = document.getElementById("quest1").children;
    // var mona = "";
    // mona = c[1].textContent;

    // console.log(mona);
    // // c[1].textContent= "";
    // console.log("value"+ c[1].textContent);
    // var txt = "";
    // var i;
    // for (i = 0; i < c.length; i++) {
    //   txt = txt + c[i].getAttribute("id") + "<br>";
    // }
  
    // document.getElementById("demo").innerHTML = txt;
  


