function targetDropDown(controlName, serviceName) {	
    $("#" + controlName).select2({
        ajax: {
            url: "../DropDownSources/" + serviceName + ".php" + "?conparam=" + sessionStorage.getItem("conparam"),
            type: "post",
            dataType: 'json',
            delay: 250,
            data: function(params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function(response) {
                return {
                    results: response
                };
            },
            cache: true
        }
    });
    $("#" + controlName).select2("val", "");
}

function targetDropDownWithAddingNewItem(controlName, serviceName) {
    $("#" + controlName).select2({
		tags: true,
        ajax: {
            url: "../DropDownSources/" + serviceName + ".php" + "?conparam=" + sessionStorage.getItem("conparam"),
            type: "post",
            dataType: 'json',
            delay: 250,
            data: function(params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function(response) {
                return {
                    results: response
                };
            },
            cache: true
        }
    });
    $("#" + controlName).select2("val", "");
}

function getMultipleSelectedValues(select, serviceName, row_id) {
	var selectResult = '{'
    var options = select && select.options;
    var opt;

    for (var i=0, iLen=options.length; i<iLen; i++) {
		opt = options[i];
		if (opt.selected) {
			selectResult += '"' + opt.value + '":"' + opt.text + '",';
		}
    }
	selectResult = selectResult.substring(0, selectResult.length - 1);
	selectResult += '}';
	var jsonResult = '{"ID":"' + row_id + '","Engineers":[' + selectResult + ']}';
    //details_submitData(jsonResult, serviceName, "edit");
}